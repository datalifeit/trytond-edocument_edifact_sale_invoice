# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import invoice, edocument


def register():
    Pool.register(
        invoice.Invoice,
        invoice.InvoiceLine,
        invoice.Message,
        edocument.EdocumentTemplate,
        edocument.ConfigurationPath,
        module='edocument_edifact_sale_invoice', type_='model')
    Pool.register(
        invoice.CreateEDIFACTINVOIC,
        module='edocument_edifact_sale_invoice', type_='wizard')
    Pool.register(
        module='edocument_edifact_sale_invoice', type_='report')
