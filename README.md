datalife_edocument_edifact_sale_invoice
=======================================

The edocument_edifact_sale_invoice module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-edocument_edifact_sale_invoice/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-edocument_edifact_sale_invoice)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
