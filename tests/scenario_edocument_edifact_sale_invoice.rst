=======================================
Edocument Edifact Sale Invoice Scenario
=======================================

Imports::

    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules

Install edocument_edifact_sale_invoice::

    >>> config = activate_modules('edocument_edifact_sale_invoice')
