# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.wizard import Wizard
from trytond.modules.edocument_edifact.edocument import (
    EdocumentMixin, EdocumentExportMixin)


class Message(metaclass=PoolMeta):
    __name__ = 'edocument.message'

    @classmethod
    def _get_origin(cls):
        return super(Message, cls)._get_origin() + ['account.invoice']


class Invoice(EdocumentMixin, metaclass=PoolMeta):
    __name__ = 'account.invoice'

    @property
    def customer_reference(self):
        origins = set([line.origin for line in self.lines if line.origin])
        if not origins:
            return ''
        origin = origins.pop()
        if self.total_amount >= 0:
            return origin.sale.shipments[0].number
        else:
            if origin.__name__ == 'sale.cost':
                if origin.sale.invoices:
                    return origin.sale.invoices[0].number
            else:
                return origin.invoice.number
        return ''

    def get_delivery_ean(self):
        if self.total_amount >= 0:
            origins = list(set(
                [l.origin.sale for l in self.lines if l.origin]))
            if len(origins) == 1:
                return origins[0].shipment_address.edi_ean
            if self.invoice_address and self.invoice_address.edi_ean:
                return self.invoice_address.edi_ean
        else:
            if self.invoice_address and self.invoice_address.edi_ean:
                return self.invoice_address.edi_ean
        codes = [id.code for id in self.party.identifiers
            if id.type == 'EDI_receiver']
        return codes[0] if codes else None


class InvoiceLine(metaclass=PoolMeta):
    __name__ = 'account.invoice.line'

    def get_reference(self):
        pattern = {
            'party': self.invoice.party.id,
            'address': self.invoice.invoice_address.id
        }
        return self.product.get_cross_reference(pattern)

    def product_ean_code(self):
        reference = self.get_reference()
        if reference:
            return reference.ean_code
        return self.product.ean_code

    def product_code(self):
        reference = self.get_reference()
        if reference:
            return reference.code
        return self.product.code

    def product_name(self):
        reference = self.get_reference()
        if reference:
            return reference.name
        return self.product.name


class CreateEDIFACTINVOIC(EdocumentExportMixin, Wizard):
    """Create EDIFACT INVOIC"""
    __name__ = 'account.invoice.create_edifact_invoic'
    _message_type = 'INVOIC'

    @classmethod
    def _default_template_path_file(cls):
        return __file__
